from django.shortcuts import render
from django.http import HttpResponseRedirect
from .Forms import Activity
from .models import Activity as Activ

response = {}

# Create your views here.
def web(request):
	args={}
	return render(request, 'web/home.html', args)

def contactMe(request):
	return render(request, 'web/BukuTamu.html', {})

def form(request):
	response['fill_form'] = Activity
	return render(request, 'web/Form.html', response)

def fill_form(request):
	form = Activity(request.POST or None)
	if (request.method == 'POST'):
		response['name'] = request.POST['name']
		response['place'] = request.POST['place']
		response['category'] = request.POST['category']
		response['time'] = request.POST['time']
		jadwal = Activ()
		jadwal.name = request.POST['name']
		jadwal.category = request.POST['category']
		jadwal.place = request.POST['place']
		jadwal.time = request.POST['time']
		jadwal.save()
		html = 'web/Form.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/')

def table(request):
	schedule = Activ.objects.all()
	response['table'] = schedule
	return render(request, 'web/table.html', response)

def purge(request):
	Activ.objects.all().delete()
	return HttpResponseRedirect('table')