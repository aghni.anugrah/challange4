from django import forms

class Activity(forms.Form):
    attrs = {
        'class' : 'form-control'
    }

    name = forms.CharField(label = "name", required = True, widget = forms.TextInput(attrs = attrs))
    category = forms.CharField(label = "category", required = True, widget = forms.TextInput(attrs = attrs))
    place = forms.CharField(label = "place of event", required = True, widget = forms.TextInput(attrs = attrs))
    time = forms.DateTimeField(label = "time of event", required = True, widget = forms.TextInput(attrs = {'type':'datetime-local', 'class':'form-control'}))